microk8s.kubectl delete -f test-k8s/echo-k8s-deployment.yaml
microk8s.kubectl delete -f test-k8s/nginx-config.yaml
microk8s.kubectl delete -f api-gate/api-gate-deployment.yaml
microk8s.kubectl delete -f api-gate/api-gate-config.yaml
microk8s.kubectl delete -f backend/backend-deployment.yaml
microk8s.kubectl get svc
microk8s.kubectl get pods
