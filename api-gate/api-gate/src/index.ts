import http, {IncomingMessage, ServerResponse} from 'http';
import {parse} from 'url';
import fetch from 'node-fetch';
//import { rootCertificates } from 'tls';

const BACKEND1 = 'http://backend-adapter:3000';
const BACKEND2 = 'http://backend-adapter:3001';

const DATABASE = 1;
const STORAGE = 2;

const BAD_REQUEST = 400;
const UNAUTHORIZED = 401;
const NOT_FOUND = 404;

const convTable: {[origin: string]: string} = {
	'articles': 'article',
	'article-templates': 'article_template',
	'article-sample-templates': 'article_sample_template',
};

const checkRequest = (uri: string): [number, string] => {
	const uriArray = uri.split('/');
	if (uriArray[1] != 'beta1') {
		return [0, ""];
	}
	if (uriArray[2] == 'files') {
		if (uriArray.length != 4) {
			return [0, ""];
		}
		return [STORAGE, BACKEND2 + '/beta1/' + uriArray[3]];
	}
	const api: string = convTable[uriArray[2]];
	if (api == undefined) {
		return [0, ""];
	}
	let rc: string = "";
	uriArray[2] = api;
	uriArray.shift();
	uriArray.forEach(item => {
		rc += '/';
		rc += item;
	});
	return [DATABASE, BACKEND1 + rc];
}

const postJson = async (url: string, body: string): Promise<[number, any]> => {
	try {
		const response = await fetch(url, {
			method: 'POST',
			body: body,
			headers: {
				'Content-Type': 'application/json; charset=utf-8',
				'Access-Control-Allow-Origin': '*'
			}
		});
		const cnt = await response.buffer();
		const rc: number = await response.status;
		return [rc, cnt];
	} catch (error) {
		console.error(error);
		return [NOT_FOUND, ""];
	}
};

const getJson = async (url: string): Promise<[number, any]> => {
	try {
		const response = await fetch(url, {
			method: 'GET',
			headers: {
				'connection': 'keep-alive',
				'Access-Control-Allow-Origin': '*'
			}
		});
		const body = await response.buffer();
		const rc: number = await response.status;
		return [rc, body];
	} catch (error) {
		console.error(error);
		return [NOT_FOUND, ""];
	}
};

const postJpeg = async (url: string, body: any): Promise<[number, any]> => {
	try {
		const response = await fetch(url, {
			method: 'POST',
			body: body,
			headers: {
				'Content-Type': 'image/jpeg',
				'Access-Control-Allow-Origin': '*'
			}
		});
		const cnt = await response.buffer();
		const rc: number = await response.status;
		return [rc, cnt];
	} catch (error) {
		console.error(error);
		return [NOT_FOUND, ""];
	}
};

const getJpeg = async (url: string): Promise<[number, any]> => {
	try {
		const response = await fetch(url, {
			method: 'GET',
			headers: {
				'connection': 'keep-alive',
				'Access-Control-Allow-Origin': '*'
			}
		});
		const body = await response.buffer();
		const rc: number = await response.status;
		return [rc, body];
	} catch (error) {
		console.error(error);
		return [NOT_FOUND, ""];
	}
};

const server = http.createServer();

server.on('request', function(request, response) {
	let url = parse(request.url || "", true);
	response.writeHead(200, {'content-Type':'text/html; charset=UTF-8'});
	response.write(url);
	response.end();
/*
	let auth = request.headers['authorization'];
	let body: any = [];
	let bodyStr: string;
	request.on('data', (chunk: any) => {
		body.push(chunk);
		console.log("chunk");
	}).on('end', async () => {
		let rc: number;
		let rcbody: any;
		bodyStr = Buffer.concat(body).toString();
		let res: number;
		let api: string;
		switch ((request.method).toString()) {
			case 'GET':
				[res, api] = checkRequest(url.href.toString());
				switch (res) {
					case DATABASE:
						[rc, rcbody] = await getJson(api);
						response.writeHead(rc, {'content-Type':'application/json; charset=UTF-8', 'Access-Control-Allow-Origin': '*'});
						response.write(rcbody);
						response.end();
						break;
					case STORAGE:
						[rc, rcbody] = await getJpeg(api);
						response.writeHead(rc, {'content-Type':'image/jpeg', 'Access-Control-Allow-Origin': '*'});
						response.write(rcbody);
						response.end();
						break;
				}
				break;
			case 'POST':
				[res, api] = checkRequest(url.href.toString());
				switch (res) {
					case DATABASE:
						[rc, rcbody] = await postJson(api, bodyStr);
						response.writeHead(rc, {'content-Type':'application/json; charset=UTF-8', 'Access-Control-Allow-Origin': '*'});
						response.write(rcbody);
						response.end();
						break;
					case STORAGE:
						[rc, rcbody] = await postJpeg(api, body);
						response.writeHead(rc, {'content-Type':'application/json; charset=UTF-8', 'Access-Control-Allow-Origin': '*'});
						response.write(rcbody);
						response.end();
						break;
				}
				break;
			default:
				response.writeHead(NOT_FOUND, {'content-Type':'application/json; charset=UTF-8'});
				response.end();
		break;
		}
		}).on('error', (e: any) => {
		console.error(e);
		response.writeHead(BAD_REQUEST, {'content-Type':'application/json; charset=UTF-8'});
		response.end();
});
*/
});

server.listen(10080); // 10080番ポートで起動
