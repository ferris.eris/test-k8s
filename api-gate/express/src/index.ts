import express from 'express';
import axios, { AxiosResponse } from 'axios';

const BACKEND: string = 'http://localhost:3000';

const app: express.Express = express();

// cors
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(express.json());
app.use(express.urlencoded({ extended: true }));


//routing
const router: express.Router = express.Router();
router.get('/beta1/articles/', (req:express.Request, res:express.Response, next:express.NextFunction) => {
  (async () => {
    const ares: AxiosResponse = await axios.get(BACKEND+'/beta1/articles/');
    res.send(ares.data);
  })().catch(next);
});

router.get('/beta1/articles/:id', (req:express.Request, res:express.Response, next:express.NextFunction) => {
  (async () => {
    const ares: AxiosResponse = await axios.get(BACKEND+'/beta1/articles/'+req.params.id);
    res.send(ares.data);
  })().catch(next);
});

router.post('/beta1/articles/', (req:express.Request, res:express.Response, next:express.NextFunction) => {
  (async () => {
    const ares: AxiosResponse = await axios.post(BACKEND+'/beta1/articles/'+req.params.id, JSON.parse(JSON.stringify(req.body)));
    res.send(ares.data);
  })().catch(next);
});
app.use(router);

app.listen(10080, () => { console.log("Start"); });